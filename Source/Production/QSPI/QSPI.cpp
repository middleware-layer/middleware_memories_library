/**
 ******************************************************************************
 * @file    QSPI.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    19 de jun de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/QSPI/QSPI.hpp>

namespace Memories
{
   QSPI::QSPI()
   {
      this->aRxBuffer.clear();

      BSP_QSPI_Init();
   }

   QSPI::~QSPI()
   {
      // TODO Auto-generated destructor stub
   }

   auto QSPI::write(uint8_t* _data,
                    uint32_t _writeAddr,
                    uint32_t _size/*std::string _data, uint32_t _writeAddr*/) -> uint8_t
   {
      _writeAddr -= QSPI_BASE_ADDR;

      if ((_writeAddr % (uint32_t) 0x10000) == 0)
      {
         BSP_QSPI_Erase_Block(_writeAddr);
      }

      return BSP_QSPI_Write(_data,
                            _writeAddr,
                            _size);
   }

   auto QSPI::read(std::string _data,
                   uint32_t _readAddr,
                   uint32_t _size) -> uint8_t
   {
      _readAddr -= QSPI_BASE_ADDR;

      return BSP_QSPI_Read((uint8_t *) _data.c_str(),
                           _readAddr,
                           _size);
   }
}   // namespace External_Memories

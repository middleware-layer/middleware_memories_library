/**
 ******************************************************************************
 * @file    QSPI.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    19 de jun de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_QSPI_QSPI_HPP
#define SOURCE_PRODUCTION_QSPI_QSPI_HPP

#include <string>

#ifdef MIDDLEWARE_USE_STM32F7
#include <Production/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal.h>
#endif

#ifdef USE_STM32F769I_DISCO
#include <Production/BSP/STM32F769I-Discovery/Inc/stm32f769i_discovery_qspi.h>
#endif

#define BUFFER_SIZE         ((uint32_t)0x0200)
#define WRITE_READ_ADDR     ((uint32_t)0x0000)
#define QSPI_BASE_ADDR     	((uint32_t)0x90000000)

namespace Memories
{
   class QSPI
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         std::string aRxBuffer;

         /**
          * @brief
          */
         QSPI();

         /**
          * @brief
          */
         virtual ~QSPI();

         /**
          * @brief
          * @param _data
          * @param _writeAddr
          * @param _size
          * @return
          */
         auto write(uint8_t* _data,
                    uint32_t _writeAddr,
                    uint32_t _size) -> uint8_t;

         /**
          * @brief
          * @param _data
          * @param _readAddr
          * @param _size
          * @return
          */
         auto read(std::string _data,
                   uint32_t _readAddr,
                   uint32_t _size) -> uint8_t;
   };
}

#endif // SOURCE_PRODUCTION_QSPI_QSPI_HPP

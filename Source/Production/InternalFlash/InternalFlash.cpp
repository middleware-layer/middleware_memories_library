/**
 ******************************************************************************
 * @file    InternalFlash.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    18 de ago de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/InternalFlash/InternalFlash.hpp>

namespace Memories
{
   InternalFlash::InternalFlash()
   {
      HAL_FLASH_Unlock();                       // Unlock the Flash module

      HAL_FLASH_OB_Unlock();                    // Allow Access to option bytes sector

      HAL_FLASHEx_OBGetConfig(&this->OBInit);   // Get the Dual bank configuration status
   }

   InternalFlash::~InternalFlash()
   {
      this->lockFlash();
   }

   auto InternalFlash::lockFlash(void) -> void
   {
      HAL_FLASH_OB_Launch();

      HAL_FLASH_OB_Lock();   // Prevent Access to option bytes sector

      HAL_FLASH_Lock();   // Disable the Flash option control register access (recommended to protect the option Bytes against possible unwanted operations)
   }

   auto InternalFlash::getSector(uint32_t _address) -> uint32_t
   {
      uint32_t sector = 0;

      if ((_address < ADDR_FLASH_SECTOR_1) && (_address >= ADDR_FLASH_SECTOR_0))
      {
         sector = FLASH_SECTOR_0;
      }

      else if ((_address < ADDR_FLASH_SECTOR_2) && (_address >= ADDR_FLASH_SECTOR_1))
      {
         sector = FLASH_SECTOR_1;
      }

      else if ((_address < ADDR_FLASH_SECTOR_3) && (_address >= ADDR_FLASH_SECTOR_2))
      {
         sector = FLASH_SECTOR_2;
      }

      else if ((_address < ADDR_FLASH_SECTOR_4) && (_address >= ADDR_FLASH_SECTOR_3))
      {
         sector = FLASH_SECTOR_3;
      }

      else if ((_address < ADDR_FLASH_SECTOR_5) && (_address >= ADDR_FLASH_SECTOR_4))
      {
         sector = FLASH_SECTOR_4;
      }

      else if ((_address < ADDR_FLASH_SECTOR_6) && (_address >= ADDR_FLASH_SECTOR_5))
      {
         sector = FLASH_SECTOR_5;
      }

      else if ((_address < ADDR_FLASH_SECTOR_7) && (_address >= ADDR_FLASH_SECTOR_6))
      {
         sector = FLASH_SECTOR_6;
      }

      else if ((_address < ADDR_FLASH_SECTOR_8) && (_address >= ADDR_FLASH_SECTOR_7))
      {
         sector = FLASH_SECTOR_7;
      }

      else if ((_address < ADDR_FLASH_SECTOR_9) && (_address >= ADDR_FLASH_SECTOR_8))
      {
         sector = FLASH_SECTOR_8;
      }

      else if ((_address < ADDR_FLASH_SECTOR_10) && (_address >= ADDR_FLASH_SECTOR_9))
      {
         sector = FLASH_SECTOR_9;
      }

      else if ((_address < ADDR_FLASH_SECTOR_11) && (_address >= ADDR_FLASH_SECTOR_10))
      {
         sector = FLASH_SECTOR_10;
      }

#if defined(DUAL_BANK)
      else if ((_address < ADDR_FLASH_SECTOR_12) && (_address >= ADDR_FLASH_SECTOR_11))
      {
         sector = FLASH_SECTOR_11;
      }

      else if ((_address < ADDR_FLASH_SECTOR_13) && (_address >= ADDR_FLASH_SECTOR_12))
      {
         sector = FLASH_SECTOR_12;
      }

      else if ((_address < ADDR_FLASH_SECTOR_14) && (_address >= ADDR_FLASH_SECTOR_13))
      {
         sector = FLASH_SECTOR_13;
      }

      else if ((_address < ADDR_FLASH_SECTOR_15) && (_address >= ADDR_FLASH_SECTOR_14))
      {
         sector = FLASH_SECTOR_14;
      }

      else if ((_address < ADDR_FLASH_SECTOR_16) && (_address >= ADDR_FLASH_SECTOR_15))
      {
         sector = FLASH_SECTOR_15;
      }

      else if ((_address < ADDR_FLASH_SECTOR_17) && (_address >= ADDR_FLASH_SECTOR_16))
      {
         sector = FLASH_SECTOR_16;
      }

      else if ((_address < ADDR_FLASH_SECTOR_18) && (_address >= ADDR_FLASH_SECTOR_17))
      {
         sector = FLASH_SECTOR_17;
      }

      else if ((_address < ADDR_FLASH_SECTOR_19) && (_address >= ADDR_FLASH_SECTOR_18))
      {
         sector = FLASH_SECTOR_18;
      }

      else if ((_address < ADDR_FLASH_SECTOR_20) && (_address >= ADDR_FLASH_SECTOR_19))
      {
         sector = FLASH_SECTOR_19;
      }

      else if ((_address < ADDR_FLASH_SECTOR_21) && (_address >= ADDR_FLASH_SECTOR_20))
      {
         sector = FLASH_SECTOR_20;
      }

      else if ((_address < ADDR_FLASH_SECTOR_22) && (_address >= ADDR_FLASH_SECTOR_21))
      {
         sector = FLASH_SECTOR_21;
      }

      else if ((_address < ADDR_FLASH_SECTOR_23) && (_address >= ADDR_FLASH_SECTOR_22))
      {
         sector = FLASH_SECTOR_22;
      }

      else   // (_address < FLASH_END_ADDR) && (_address >= ADDR_FLASH_SECTOR_23)
      {
         sector = FLASH_SECTOR_23;
      }
#else
      else   // (_address < FLASH_END_ADDR) && (_address >= ADDR_FLASH_SECTOR_11)
      {
         sector = FLASH_SECTOR_11;
      }
#endif // DUAL_BANK

      return sector;
   }

   auto InternalFlash::eraseSector(uint32_t _sector) -> bool
   {
      FLASH_EraseInitTypeDef EraseInitStruct;

      uint32_t SECTORError = 0;

      // Fill EraseInit structure
      EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
      EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
      EraseInitStruct.Sector = _sector;
      EraseInitStruct.NbSectors = 1;

      /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
       you have to make sure that these data are rewritten before they are accessed during code
       execution. If this cannot be done safely, it is recommended to flush the caches by setting the
       DCRST and ICRST bits in the FLASH_CR register. */
      if (HAL_FLASHEx_Erase(&EraseInitStruct,
                            &SECTORError) != HAL_OK)
      {
         return false;
      }

      return true;
   }

   auto InternalFlash::eraseBank(BANK _bank) -> bool
   {
       bool retVal = false;
#ifdef DUAL_BANK
       FLASH_EraseInitTypeDef EraseInitStruct;

      uint32_t SECTORError = 0;
      uint32_t FirstSector;
      uint32_t NbOfSectors;

      if (_bank == BANK::BANK_1)   //   erase Bank 1
      {
         FirstSector = getSector(ADDR_FLASH_SECTOR_0);                          // Get the 1st sector to erase

         NbOfSectors = getSector(ADDR_FLASH_SECTOR_11 - 1) - FirstSector + 1;   // Get the number of sector to erase from 1st sector
      }

      else   // erase Bank 2
      {
         FirstSector = getSector(ADDR_FLASH_SECTOR_12);                         // Get the 1st sector to erase

         NbOfSectors = getSector(ADDR_FLASH_SECTOR_23 - 1) - FirstSector + 1;   // Get the number of sector to erase from 1st sector
      }

      // Fill EraseInit structure
      EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
      EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
      EraseInitStruct.Sector = FirstSector;
      EraseInitStruct.NbSectors = NbOfSectors;

      /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
       you have to make sure that these data are rewritten before they are accessed during code
       execution. If this cannot be done safely, it is recommended to flush the caches by setting the
       DCRST and ICRST bits in the FLASH_CR register. */
      retVal = HAL_FLASHEx_Erase(&EraseInitStruct,
                                 &SECTORError) == HAL_OK;
#endif

      return retVal;
   }

   auto InternalFlash::write(__IO uint32_t* _flashAddress,
                                  uint32_t* _data,
                                  uint16_t _dataLength) -> uint8_t
   {
      uint32_t limitAddress = *_flashAddress + _dataLength;

      uint32_t lastBankValidAddress;

#ifdef DUAL_BANK
      (this->getBank() == BANK::BANK_1) ? (lastBankValidAddress = ADDR_FLASH_SECTOR_23) :
                                          (lastBankValidAddress = ADDR_FLASH_SECTOR_11);
#else
      lastBankValidAddress = ADDR_FLASH_SECTOR_11;
#endif

      for (uint32_t i = 0; (i < _dataLength) && (*_flashAddress <= (lastBankValidAddress - 4)); i++)
      {
         if (*_flashAddress == limitAddress)
         {
            return 0;
         }

         // Device voltage range supposed to be [2.7V to 3.6V], the operation will be done by word
         if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,
                               *_flashAddress,
                               *(uint32_t*) (_data + i)) == HAL_OK)
         {
            *_flashAddress += 4;   // Increment FLASH destination address
         }

         else
         {
            return 1;   // Error occurred while writing data in Flash memory
         }
      }

      return 0;
   }

   auto InternalFlash::copyBanks(__IO uint32_t* _dstFlashAddress,
                                 uint32_t _srcFlashAddress) -> int8_t
   {
       int8_t retVal = 1;

#ifdef DUAL_BANK
       retVal = 0;

       uint32_t DataLength = (ADDR_FLASH_SECTOR_23 - ADDR_FLASH_SECTOR_11);

       for (uint32_t i = 0; (i < DataLength) && (retVal == 0); i += 4)
       {
           uint64_t *converter = reinterpret_cast<uint64_t *>(_srcFlashAddress);

           // Device voltage range supposed to be [2.7V to 3.6V], the operation will be done by word
           if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,
                                 *_dstFlashAddress,
                                 converter[0]) == HAL_OK)
           {
               *_dstFlashAddress += 4;   // Increment FLASH destination address

               _srcFlashAddress += 4;// Increment FLASH source address
           }

           else
           {
               retVal = 1;   // Error occurred while writing data in Flash memory
           }
       }
#endif

       return retVal;
   }

   auto InternalFlash::setDeviceSerialNumber(uint32_t _dstFlashAddress,
                                             uint32_t *_serialNumber) -> bool
   {
      if (this->write(&_dstFlashAddress,
                      _serialNumber,
                      1) != HAL_OK)
      {
         return false;
      }

      return true;
   }

   auto InternalFlash::getBank(void) -> BANK
   {
      // Switch Boot bank: the BootAddrx value "0x2040" corresponds to bank2 address: "0x08100000"
      if (((__HAL_SYSCFG_GET_BOOT_MODE() == SYSCFG_MEM_BOOT_ADD0) && (OBInit.BootAddr0 < 0x2040)) || ((__HAL_SYSCFG_GET_BOOT_MODE() == SYSCFG_MEM_BOOT_ADD1) && (OBInit.BootAddr1 < 0x2040)))
      {
         return BANK::BANK_1;
      }

      return BANK::BANK_2;
   }

   auto InternalFlash::changeRunningBank(void) -> void
   {
      if (this->getBank() == BANK::BANK_1)
      {
         OBInit.OptionType = OPTIONBYTE_BOOTADDR_0 | OPTIONBYTE_BOOTADDR_1;
         OBInit.BootAddr0 = __HAL_FLASH_CALC_BOOT_BASE_ADR(0x08100000);
         OBInit.BootAddr1 = __HAL_FLASH_CALC_BOOT_BASE_ADR(0x08100000);
      }

      else
      {
         OBInit.OptionType = OPTIONBYTE_BOOTADDR_0 | OPTIONBYTE_BOOTADDR_1;
         OBInit.BootAddr0 = __HAL_FLASH_CALC_BOOT_BASE_ADR(0x08000000);
         OBInit.BootAddr1 = __HAL_FLASH_CALC_BOOT_BASE_ADR(0x08000000);
      }

      HAL_FLASHEx_OBProgram(&this->OBInit);

      this->lockFlash();

      return;
   }
}   // namespace External_Memories

/**
 ******************************************************************************
 * @file    InternalFlash.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    18 de ago de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_INTERNALFLASH_INTERNALFLASH_HPP
#define SOURCE_PRODUCTION_INTERNALFLASH_INTERNALFLASH_HPP

#include <string>
#include <stdlib.h>

#ifdef MIDDLEWARE_USE_STM32F7
#include <Production/STM32F7xx_HAL_Driver/Inc/stm32f7xx_hal.h>

#ifdef USE_STM32F769I_DISCO
#include <Production/BSP/STM32F769I-Discovery/Inc/flash_data.h>
#endif
#endif

namespace Memories
{
   /**
    * @brief  This class will handle everything related to the internal flash memory
    *		   One should take care about locking the flash before a system reset (we
    *		   lock it inside the destructor, but if a system reset occurs earlier than
    *		   the destructor is called, one should call the 'lockFlash()' method!
    */
   class InternalFlash
   {
      private:
         /**
          * @brief  This struct stores the option bytes for the internal flash
          */
         FLASH_OBProgramInitTypeDef OBInit;

         /**
          * @brief  Gets the sector of a given address
          * @retval The sector of a given address
          */
         auto getSector(uint32_t _address) -> uint32_t;

      protected:

      public:
         enum class BANK
         {
            BANK_1 = FLASH_BANK_1,
            BANK_2 = FLASH_BANK_2
         };

         /**
          * @brief  At start, we only unlock the flash and get its option bytes
          */
         InternalFlash();

         /**
          * @brief  When the destructor is called, it means that we are no longer using the flash,
          *          so we need to lock it to prevent errors
          */
         virtual ~InternalFlash();

         /**
          * @brief  We need to lock the internal flash memory to prevent unwanted operations inside it
          */
         auto lockFlash(void) -> void;

         /**
          * @brief  This function writes a data buffer in flash (data are 32-bit aligned).
          * @note   After writing data buffer, the flash content is checked.
          * @param  _flashAddress :  start address for writing data buffer
          * @param  _data         :  pointer on data buffer
          * @param  _dataLength   :  length of data buffer (unit is 32-bit word)
          * @retval 0             :  Data successfully written to Flash memory
          *         1             :  Error occurred while writing data in Flash memory
          *         2             :  Written Data in flash memory is different from expected one
          */
         auto write(__IO uint32_t* _flashAddress,
                         uint32_t* _data,
                         uint16_t _dataLength) -> uint8_t;

         /**
          * @brief  This function does an erase of one flash sector
          * @param  StartSector     :  start of user flash area
          * @retval true         :  user flash area successfully erased
          *         false        :  error occured
          */
         auto eraseSector(uint32_t _sector) -> bool;

         /**
          * @brief  This function does an erase of all user flash area
          * @param  StartSector  :  start of user flash area
          * @retval true      :  user flash area successfully erased
          *         false     :  error occured
          */
         auto eraseBank(BANK _bank) -> bool;

         /**
          * @brief  This function copies an entire bank to another.
          * @param  _dstFlashAddress   :  The start destination bank address
          * @param  _srcFlashAddress   :  The start source bank address
          * @retval 0               :  Data successfully written to Flash memory
          *         1               :  Error occurred while writing data in Flash memory
          *         2               :  Written Data in flash memory is different from expected one
          */
         auto copyBanks(__IO uint32_t* _dstFlashAddress,
                        uint32_t _srcFlashAddress) -> int8_t;

         /**
          * @brief  This emthod will write to _dstFlashAddress a serial number (32 bits)
          * @param  _dstFlashAddress   :  desired flash address
          * @param  _serialNumber      :  Serial number to be written
          * @retval true            :  Data successfully written to Flash memory
          *         false           :  Error occurred while writing data in Flash memory
          */
         auto setDeviceSerialNumber(uint32_t _dstFlashAddress,
                                    uint32_t* _serialNumber) -> bool;

         /**
          * @brief  This method will read whick bank the system is currently on and return it
          * @rerval BANK the system is on
          */
         auto getBank(void) -> BANK;

         /**
          * @brief  This method will write the option bytes to the OBInit struct.
          *          It will only change the current running bank
          */
         auto changeRunningBank(void) -> void;
   };
}

#endif // SOURCE_PRODUCTION_INTERNALFLASH_INTERNALFLASH_HPP

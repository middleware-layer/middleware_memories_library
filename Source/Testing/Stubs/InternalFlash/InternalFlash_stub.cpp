/**
 ******************************************************************************
 * @file    InternalFlash_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/InternalFlash/InternalFlash_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Memories
{
    static I_InternalFlash* p_InternalFlash_Impl = NULL;

    const char* InternalFlash_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle InternalFlash functions";
    }

    void stub_setImpl(I_InternalFlash* pNewImpl)
    {
        p_InternalFlash_Impl = pNewImpl;
    }

    static I_InternalFlash* InternalFlash_Stub_Get_Impl(void)
    {
        if(p_InternalFlash_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to InternalFlash functions. Did you forget" << "to call Stubs::Memories::stub_setImpl() ?"
                    << std::endl;

            throw InternalFlash_StubImplNotSetException();
        }

        return p_InternalFlash_Impl;
    }

    ////////////////////////////////////////
    // I_InternalFlash Methods Definition //
    ////////////////////////////////////////

    I_InternalFlash::~I_InternalFlash()
    {
        if(p_InternalFlash_Impl == this)
        {
            p_InternalFlash_Impl = NULL;
        }
    }

    ////////////////////////////////////////////
    // _Mock_InternalFlash Methods Definition //
    ////////////////////////////////////////////

    _Mock_InternalFlash::_Mock_InternalFlash()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    InternalFlash::InternalFlash()
    {
    }

    InternalFlash::~InternalFlash()
    {
    }

    auto InternalFlash::lockFlash(void) -> void
    {
        return InternalFlash_Stub_Get_Impl()->lockFlash();
    }

    auto InternalFlash::getSector(uint32_t _address) -> uint32_t
    {
        return InternalFlash_Stub_Get_Impl()->getSector(_address);
    }

    auto InternalFlash::eraseSector(uint32_t _sector) -> bool
    {
        return InternalFlash_Stub_Get_Impl()->eraseSector(_sector);
    }

    auto InternalFlash::eraseBank(BANK _bank) -> bool
    {
        return InternalFlash_Stub_Get_Impl()->eraseBank(_bank);
    }

    auto InternalFlash::write(__IO uint32_t* _flashAddress, uint32_t* _data, uint16_t _dataLength) -> uint8_t
    {
        return InternalFlash_Stub_Get_Impl()->write(_flashAddress, _data, _dataLength);
    }

    auto InternalFlash::copyBanks(__IO uint32_t* _dstFlashAddress, uint32_t _srcFlashAddress) -> int8_t
    {
        return InternalFlash_Stub_Get_Impl()->copyBanks(_dstFlashAddress, _srcFlashAddress);
    }

    auto InternalFlash::setDeviceSerialNumber(uint32_t _dstFlashAddress, uint32_t *_serialNumber) -> bool
    {
        return InternalFlash_Stub_Get_Impl()->setDeviceSerialNumber(_dstFlashAddress, _serialNumber);
    }

    auto InternalFlash::getBank(void) -> BANK
    {
        return InternalFlash_Stub_Get_Impl()->getBank();
    }

    auto InternalFlash::changeRunningBank(void) -> void
    {
        return InternalFlash_Stub_Get_Impl()->changeRunningBank();
    }
}


/**
 ******************************************************************************
 * @file    InternalFlash_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_INTERNALFLASH_INTERNALFLASH_STUB_HPP
#define SOURCE_TESTING_STUBS_INTERNALFLASH_INTERNALFLASH_STUB_HPP

#include <Production/InternalFlash/InternalFlash.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Memories
{
   class InternalFlash_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_InternalFlash
   {
      public:
         virtual ~I_InternalFlash();

         virtual void lockFlash(void) = 0;

         virtual uint32_t getSector(uint32_t _address) = 0;

         virtual uint8_t write(__IO uint32_t* _flashAddress,
                                    uint32_t* _data,
                                    uint16_t  _dataLength) = 0;

         virtual bool eraseSector(uint32_t _sector) = 0;

         virtual bool eraseBank(InternalFlash::BANK _bank) = 0;

         virtual int8_t copyBanks(__IO uint32_t* _dstFlashAddress,
                                       uint32_t _srcFlashAddress) = 0;

         virtual bool setDeviceSerialNumber(uint32_t _dstFlashAddress,
                                            uint32_t* _serialNumber) = 0;

         virtual InternalFlash::BANK getBank(void) = 0;

         virtual void changeRunningBank(void) = 0;
   };

   class _Mock_InternalFlash : public I_InternalFlash
   {
      public:
         _Mock_InternalFlash();

         MOCK_METHOD0(lockFlash, void ());
         MOCK_METHOD0(getBank, InternalFlash::BANK ());
         MOCK_METHOD0(changeRunningBank, void ());
         MOCK_METHOD1(getSector, uint32_t (uint32_t));
         MOCK_METHOD1(eraseSector, bool (uint32_t));
         MOCK_METHOD1(eraseBank, bool (InternalFlash::BANK));
         MOCK_METHOD2(copyBanks, int8_t (__IO uint32_t*, uint32_t));
         MOCK_METHOD2(setDeviceSerialNumber, bool (uint32_t, uint32_t*));
         MOCK_METHOD3(write, uint8_t (__IO uint32_t*, uint32_t*, uint16_t));
   };

   typedef ::testing::NiceMock<_Mock_InternalFlash> Mock_InternalFlash;

   void stub_setImpl(I_InternalFlash* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_INTERNALFLASH_INTERNALFLASH_STUB_HPP

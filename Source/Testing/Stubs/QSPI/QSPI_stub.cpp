/**
 ******************************************************************************
 * @file    QSPI_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/QSPI/QSPI_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Memories
{
    static I_QSPI* p_QSPI_Impl = NULL;

    const char* QSPI_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle QSPI functions";
    }

    void stub_setImpl(I_QSPI* pNewImpl)
    {
        p_QSPI_Impl = pNewImpl;
    }

    static I_QSPI* QSPI_Stub_Get_Impl(void)
    {
        if(p_QSPI_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to QSPI functions. Did you forget" << "to call Stubs::Memories::stub_setImpl() ?"
                    << std::endl;

            throw QSPI_StubImplNotSetException();
        }

        return p_QSPI_Impl;
    }

    ///////////////////////////////
    // I_QSPI Methods Definition //
    ///////////////////////////////

    I_QSPI::~I_QSPI()
    {
        if(p_QSPI_Impl == this)
        {
            p_QSPI_Impl = NULL;
        }
    }

    ///////////////////////////////////
    // _Mock_QSPI Methods Definition //
    ///////////////////////////////////

    _Mock_QSPI::_Mock_QSPI()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    QSPI::QSPI()
    {
    }

    QSPI::~QSPI()
    {
    }

    uint8_t QSPI::write(uint8_t* _data,
                        uint32_t _writeAddr,
                        uint32_t _size)
    {
        return QSPI_Stub_Get_Impl()->write(_data,
                                           _writeAddr,
                                           _size);
    }

    uint8_t QSPI::read(std::string _data,
                       uint32_t _readAddr,
                       uint32_t _size)
    {
        return QSPI_Stub_Get_Impl()->read(_data,
                                          _readAddr,
                                          _size);
    }
}


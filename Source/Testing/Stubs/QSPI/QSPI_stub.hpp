/**
 ******************************************************************************
 * @file    QSPI_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_QSPI_QSPI_STUB_HPP
#define SOURCE_TESTING_STUBS_QSPI_QSPI_STUB_HPP

#include <Production/QSPI/QSPI.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Memories
{
    class QSPI_StubImplNotSetException: public std::exception
    {
        public:
            virtual const char* what(void) const throw ();
    };

    class I_QSPI
    {
        public:
            virtual ~I_QSPI();

            virtual uint8_t write(uint8_t* _data, uint32_t _writeAddr, uint32_t _size) = 0;

            virtual uint8_t read(std::string _data, uint32_t _readAddr, uint32_t _size) = 0;
    };

    class _Mock_QSPI: public I_QSPI
    {
        public:
            _Mock_QSPI();

            MOCK_METHOD3(write, uint8_t (uint8_t*, uint32_t, uint32_t));
            MOCK_METHOD3(read, uint8_t (std::string, uint32_t, uint32_t));
    };

    typedef ::testing::NiceMock<_Mock_QSPI> Mock_QSPI;

    void stub_setImpl(I_QSPI* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_QSPI_QSPI_STUB_HPP
